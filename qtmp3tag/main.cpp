#include "mainwindow.h"
#include <QApplication>
#include <id3/tag.h>
#include <QDir>
#include <QFile>
#include <QtCore>
#include <QDialog>
#include <QtGui>
#include <QMessageBox>
#include <QString>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    QString FilePath = QDir::homePath();
    QDir homeDir(FilePath);

    if(!homeDir.exists()){
        qDebug() << "not a real path";
    }

    return a.exec();
}
