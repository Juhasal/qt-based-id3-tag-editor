﻿#include "dialog_tags.h"
#include "ui_dialog_tags.h"
#include <id3/tag.h>
#include <QDir>
#include <QFile>
#include <QtCore>
#include <QDialog>
#include <QtGui>
#include <QMessageBox>
#include <QString>
#include <QFileDialog>
#include <QList>
#include <QInputDialog>
#include <id3/field.h>
#include <id3/writers.h>
#include <id3/misc_support.h>
#include <QValidator>

Dialog_tags::Dialog_tags(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog_tags)
{
    ui->setupUi(this);
    //ui->listWidget_tags->addItem("test");

}

void Dialog_tags::DialogSelectedFile(QString addr)
{
    pathToFile = addr;
    qDebug() << "get selected path:" << pathToFile;
}


void Dialog_tags::readTags()
{
    QString path = getPath();
    const char* path_char = path.toStdString().c_str();

    ID3_Tag tag(path_char);
    qDebug() << "path:" << path_char;

    ID3_Frame* titleFrame = tag.Find(ID3FID_TITLE);
    ID3_Frame* albumFrame = tag.Find(ID3FID_ALBUM);
    ID3_Frame* bandFrame = tag.Find(ID3FID_LEADARTIST);
    ID3_Frame* yearFrame = tag.Find(ID3FID_YEAR);
    //ID3_Frame* lenFrame = tag.Find(ID3FID_SONGLEN);

    QString titleName = frameToText(titleFrame);
    QString albumName = frameToText(albumFrame);
    QString bandName = frameToText(bandFrame);
    QString yearName = frameToText(yearFrame);
    //QString lenName = frameToText(lenFrame);

    ui->listWidget_tags->addItem("Title:");
    ui->listWidget_tags->addItem(titleName);
    ui->listWidget_tags->addItem("Album:");
    ui->listWidget_tags->addItem(albumName);
    ui->listWidget_tags->addItem("Artist:");
    ui->listWidget_tags->addItem(bandName);
    ui->listWidget_tags->addItem("Year:");
    ui->listWidget_tags->addItem(yearName);

    //ui->listWidget_tags->addItem("Length:");
    //ui->listWidget_tags->addItem(lenName);
    //length is in microseconds?

    qDebug() << albumName;
}

QString Dialog_tags::getPath()
{
    qDebug() << "return:" << pathToFile;
    return pathToFile;

}

Dialog_tags::~Dialog_tags()
{
    delete ui;
}

QString Dialog_tags::frameToText(ID3_Frame*& frame)
{
    char readField[1024];
    QString returnName;
    ID3_Field* aField;
    ID3_Frame* aFrame;
    aFrame = frame;
    if(NULL != aFrame){
        aField = aFrame->GetField(ID3FN_TEXT);
        aField->Get(readField, 1024);
        returnName = readField;

        //check encoding:
        //ID3_TextEnc enc = aField->GetEncoding();
        //qDebug() << "encoding" << enc;

        //if encoding = 1, check if Latin-1 makes sense:
        //QString latName = QString::fromLatin1(readField);
        //qDebug() << latName;

        //Program reads 'START OF HEADING' byte on a certain mp3 file.
        //Field has a different byte order? ID3 unsynchronization?
        //Wrong endianness?
    }
    return returnName;
}

void Dialog_tags::on_listWidget_tags_itemDoubleClicked(QListWidgetItem *item)
{
    //get the tag
    bool ok; //for checking if ok pressed.
    QString path = getPath();
    const char* path_char = path.toStdString().c_str();
    ID3_Tag tag(path_char);

    qDebug() << "path: " << path << "item: " << item->text();
    QString defaultText = item->text();

    if(ui->listWidget_tags->item(3) == item){
        qDebug() << "album";
        QString text = QInputDialog::getText(this, tr("Input"),tr("Album tag:"),QLineEdit::Normal,
                                             defaultText,&ok);
        qDebug() << "input text: " << text;

        //convert text to std::string first and then to const char*
        //using text.toStdString().c_str() intermediate only converts the first character.
        std::string text_s = text.toStdString();
        const char* text_c = text_s.c_str();
        qDebug() << "write this: " << text_c;
        if (ok && !text.isEmpty()){
            ID3_AddAlbum(&tag,text_c,true);
            tag.Update();
            ui->listWidget_tags->item(3)->setText(text_c);
        }
    }
    if(ui->listWidget_tags->item(1) == item){
        qDebug() << "title";
        QString text = QInputDialog::getText(this, tr("Input"),tr("Title tag:"),QLineEdit::Normal,
                                             defaultText,&ok);
        std::string text_s = text.toStdString();
        const char* text_c = text_s.c_str();
        qDebug() << "write this: " << text_c;
        if (ok && !text.isEmpty()){
            ID3_AddTitle(&tag,text_c,true);
            tag.Update();
            ui->listWidget_tags->item(1)->setText(text_c);
        }
    }
    if(ui->listWidget_tags->item(5) == item){
        qDebug() << "artist";
        QString text = QInputDialog::getText(this, tr("Input"),tr("Artist tag:"),QLineEdit::Normal,
                                             defaultText,&ok);
        std::string text_s = text.toStdString();
        const char* text_c = text_s.c_str();
        qDebug() << "write this: " << text_c;
        if (ok && !text.isEmpty()){
            ID3_AddArtist(&tag,text_c,true);
            tag.Update();
            ui->listWidget_tags->item(5)->setText(text_c);
        }
    }
    if(ui->listWidget_tags->item(7) == item){
        qDebug() << "year";

        QString text = QInputDialog::getText(this, tr("Input"),tr("Artist tag:"),QLineEdit::Normal,
                                             defaultText,&ok);
        std::string text_s = text.toStdString();
        const char* text_c = text_s.c_str();
        qDebug() << "write this: " << text_c;

        //QValidator does not work with inputdialog, only with QLineEdit
                //QValidator *isValidNumber = new QIntValidator(1000,2100,-)
        //This would require a custom dialog.
        //Convert to string and validate in if-statement instead.

        //Validation:
        char *p; //next character after numerical value. if not NULL, the input is not numerical.
        long textInt = strtol(text_c, &p, 10);
        if (*p){
            qDebug() << "conversion failed, not numerical";
            //TODO: add text box
        }
        else{
            if (ok && !text.isEmpty() && textInt < 2100 && textInt > 1000){
                ID3_AddYear(&tag,text_c,true);
                tag.Update();
                ui->listWidget_tags->item(7)->setText(text_c);
            }
            else{qDebug() << "Not valid input";}
        }
    }
}



