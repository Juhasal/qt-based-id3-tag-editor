#ifndef DIALOG_TAGS_H
#define DIALOG_TAGS_H

#include <QDialog>
#include <id3/tag.h>
#include <QListWidgetItem>
#include <inputdialog.h>

namespace Ui {
class Dialog_tags;
}

class Dialog_tags : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog_tags(QWidget *parent = 0);
    ~Dialog_tags();
    void DialogSelectedFile(QString addr);
    QString getPath();
    void readTags();
    QString frameToText(ID3_Frame*& frame);

private slots:
    void on_listWidget_tags_itemDoubleClicked(QListWidgetItem *item);

private:
    Ui::Dialog_tags *ui;
    QString pathToFile;
    inputDialog *inDiag;

};

#endif // DIALOG_TAGS_H
