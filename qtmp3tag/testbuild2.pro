#-------------------------------------------------
#
# Project created by QtCreator 2016-07-21T14:58:04
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = testbuild2
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    dialog_tags.cpp \
    inputdialog.cpp

HEADERS  += mainwindow.h \
    dialog_tags.h \
    inputdialog.h

FORMS    += mainwindow.ui \
    dialog_tags.ui \
    inputdialog.ui

unix|win32: LIBS += -lid3
