#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "inputdialog.h"
#include <id3/tag.h>
#include <QDir>
#include <QFile>
#include <QtCore>
#include <QDialog>
#include <QtGui>
#include <QMessageBox>
#include <QString>
#include <QFileDialog>
#include <QList>
#include <QMenu>
#include <QAction>

//Declare static member outside of functions, in the cpp file instead of h.

QString FolderStructure::folder = "";

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->action_Quit,SIGNAL(triggered()),qApp,SLOT(quit()));

}

void MainWindow::openTagsDialog(){
    tagsDialog = new Dialog_tags(this);
    tagsDialog->show();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_browse_clicked()
{
    //Browse directory and read contents to the list.
    QString homeDir = QDir::homePath();
    QDir selectDir = QFileDialog::getExistingDirectory(this,tr("Open Image"),homeDir);
    QString stringPath = selectDir.path();
    FolderStructure::setFolder(stringPath);
    //qDebug() << selectDir;
    QStringList nameFilter;
    nameFilter << "*.mp3";
    QStringList filesInDir = selectDir.entryList(nameFilter);
    qDebug() << filesInDir;

    //Present file names
    //Add tag reading here?
    foreach(QString str, filesInDir)
    {
        ui->listWidget_files->addItem(str);
    }
}

void MainWindow::on_pushButton_go_clicked()
{
    //QString homeDir = QDir::homePath();
    QString selectDir = FolderStructure::getFolder();
    QString selectedFile;
    //selectedFile = ui->listWidget_files->selectedItems();
    //segfault? Add check if any selected.
    if(ui->listWidget_files->selectedItems().size() != 0){
        selectedFile = ui->listWidget_files->currentItem()->text();
    }
    else selectedFile = "";

    QString partString;
    partString = selectDir + "/" + selectedFile;
    qDebug() << "partString:" << partString;
    qDebug() << FolderStructure::getFolder();

    //open a new dialog, read the selection from listWidget.   
    tagsDialog = new Dialog_tags(this);
    tagsDialog->DialogSelectedFile(partString);
    tagsDialog->show();
    tagsDialog->readTags();
}

void MainWindow::on_action_About_2_triggered()
{
    About = new inputDialog(this);
    About->show();
}


