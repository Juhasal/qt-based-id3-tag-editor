#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>
#include <dialog_tags.h>
#include <inputdialog.h>

namespace Ui {
class MainWindow;
class FolderStructure;
}

class FolderStructure
{
private:
    static QString folder;
public:
    static void setFolder(QString f){
        folder = f;
    }
    static QString getFolder(){
        return folder;
    }
};


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void openTagsDialog();

private slots:
    void on_pushButton_browse_clicked();

    void on_pushButton_go_clicked();

    void on_action_About_2_triggered();

    //void on_action_Quit_triggered();

private:
    Ui::MainWindow *ui;
    Dialog_tags *tagsDialog;
    inputDialog *inDiag;
    inputDialog *About;
    QAction *action_Quit;

};

#endif // MAINWINDOW_H
