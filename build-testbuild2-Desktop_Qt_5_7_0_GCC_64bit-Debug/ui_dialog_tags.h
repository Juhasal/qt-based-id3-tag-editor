/********************************************************************************
** Form generated from reading UI file 'dialog_tags.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_TAGS_H
#define UI_DIALOG_TAGS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListWidget>

QT_BEGIN_NAMESPACE

class Ui_Dialog_tags
{
public:
    QDialogButtonBox *buttonBox;
    QListWidget *listWidget_tags;

    void setupUi(QDialog *Dialog_tags)
    {
        if (Dialog_tags->objectName().isEmpty())
            Dialog_tags->setObjectName(QStringLiteral("Dialog_tags"));
        Dialog_tags->resize(275, 254);
        buttonBox = new QDialogButtonBox(Dialog_tags);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(80, 210, 181, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        listWidget_tags = new QListWidget(Dialog_tags);
        listWidget_tags->setObjectName(QStringLiteral("listWidget_tags"));
        listWidget_tags->setGeometry(QRect(10, 10, 256, 191));

        retranslateUi(Dialog_tags);
        QObject::connect(buttonBox, SIGNAL(accepted()), Dialog_tags, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), Dialog_tags, SLOT(reject()));

        QMetaObject::connectSlotsByName(Dialog_tags);
    } // setupUi

    void retranslateUi(QDialog *Dialog_tags)
    {
        Dialog_tags->setWindowTitle(QApplication::translate("Dialog_tags", "Dialog", 0));
    } // retranslateUi

};

namespace Ui {
    class Dialog_tags: public Ui_Dialog_tags {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_TAGS_H
