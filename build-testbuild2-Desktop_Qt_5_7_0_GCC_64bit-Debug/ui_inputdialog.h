/********************************************************************************
** Form generated from reading UI file 'inputdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INPUTDIALOG_H
#define UI_INPUTDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>

QT_BEGIN_NAMESPACE

class Ui_inputDialog
{
public:
    QDialogButtonBox *aboutOk;
    QLabel *label;
    QLabel *label_2;

    void setupUi(QDialog *inputDialog)
    {
        if (inputDialog->objectName().isEmpty())
            inputDialog->setObjectName(QStringLiteral("inputDialog"));
        inputDialog->resize(176, 174);
        aboutOk = new QDialogButtonBox(inputDialog);
        aboutOk->setObjectName(QStringLiteral("aboutOk"));
        aboutOk->setGeometry(QRect(0, 140, 171, 32));
        aboutOk->setOrientation(Qt::Horizontal);
        aboutOk->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        label = new QLabel(inputDialog);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 30, 111, 41));
        label->setWordWrap(true);
        label_2 = new QLabel(inputDialog);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 10, 71, 16));

        retranslateUi(inputDialog);
        QObject::connect(aboutOk, SIGNAL(accepted()), inputDialog, SLOT(accept()));
        QObject::connect(aboutOk, SIGNAL(rejected()), inputDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(inputDialog);
    } // setupUi

    void retranslateUi(QDialog *inputDialog)
    {
        inputDialog->setWindowTitle(QApplication::translate("inputDialog", "Dialog", 0));
        label->setText(QApplication::translate("inputDialog", "Developed by Juha Salminen", 0));
        label_2->setText(QApplication::translate("inputDialog", "Version 0.1", 0));
    } // retranslateUi

};

namespace Ui {
    class inputDialog: public Ui_inputDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INPUTDIALOG_H
