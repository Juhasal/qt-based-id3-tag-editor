# README #

* Simple MP3 id3 tag editor
* Version 0.1

The program uses id3lib library for handling the ID3 tags. 
At the moment only the basic 'title', 'artist', 'album', and 'year'-tags are implemented.
Windows version not implemented due to incompatibilities with the outdated id3lib library.